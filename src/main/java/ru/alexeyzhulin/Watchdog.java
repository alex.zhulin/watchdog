package ru.alexeyzhulin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.alexeyzhulin.helpers.EmailHelper;
import ru.alexeyzhulin.helpers.WebServiceChecker;
import ru.alexeyzhulin.models.CheckResult;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;


@Component
public class Watchdog implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Watchdog.class);

    private static String EMAIL_SUBJECT_ERROR = "Проверка достпности сервиса - ОШИБКА";
    private static String EMAIL_SUBJECT_OK = "Проверка достпности сервиса - ОК";
    private static String EMAIL_MESSAGE_OK = "Сервис был успешно перезапущен";

    @Autowired
    EmailHelper emailHelper;
    @Autowired
    WebServiceChecker webServiceChecker;

    @Value("${serviceStartTimeout:#{null}}")
    private String serviceStartTimeout;

    @Override
    public void run(String... strings) throws Exception {
        checkService();
    }

    private void checkService() {
        log.info("Checking web service...");
        CheckResult checkResult = webServiceChecker.checkWebService();
        log.info("Checking web service... Done.");
        if (!checkResult.getResult()) {
            log.warn("Checking result is ERROR [" + checkResult.getInfo() + "]");
            try {
                emailHelper.sendAlertMessage(EMAIL_SUBJECT_ERROR, checkResult.getInfo());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            // Trying to restart service
            log.info("Trying to restart web service...");
            webServiceChecker.restartService();
            log.info("Trying to restart web service... Done.");
            // Timeout for service starting
            log.info("Taking timeout for service start...");
            try {
                TimeUnit.SECONDS.sleep(Integer.parseInt(serviceStartTimeout));
            } catch (Exception e) {
                log.error("Taking timeout error: " + e.getClass().getName() + ": " + e.getMessage());
            }
            log.info("Taking timeout for service start... Done.");
            // Checking web service again
            log.info("Checking web service after restarting...");
            checkResult = webServiceChecker.checkWebService();
            log.info("Checking web service after restarting... Done.");
            if (checkResult.getResult()) {
                log.info("Service successfully restarted");
                try {
                    emailHelper.sendAlertMessage(EMAIL_SUBJECT_OK, EMAIL_MESSAGE_OK);
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            } else {
                log.warn("Checking result AFTER RESTART is ERROR [" + checkResult.getInfo() + "]");
            }
        } else {
            log.info("Checking result is OK");
        }
    }
}

package ru.alexeyzhulin.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.alexeyzhulin.models.CheckResult;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class WebServiceChecker {

    private static final Logger log = LoggerFactory.getLogger(WebServiceChecker.class);

    @Value("${watchingUrl:#{null}}")
    private String watchingUrl;
    @Value("${scriptFile:#{null}}")
    private String scriptFile;

    public CheckResult checkWebService() {
        CheckResult checkResult = new CheckResult();

        try {
            URL url = new URL(watchingUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            con.setRequestMethod("GET");
            log.info("Sending request to: [" + watchingUrl + "]...");
            int responseCode = con.getResponseCode();
            log.info("Sending request to: [" + watchingUrl + "]...Done. Response code = [" + responseCode + "]");
            if (responseCode == 200) {
                checkResult.setResult(true);
            } else {
                checkResult.setResult(false);
                checkResult.setInfo("Ошибка достпности сервиса по адресу: [" + watchingUrl + "]; Response code: [" + responseCode + "]. Будет предпринята попытка перезапуска сервиса");
            }
        } catch (IOException e) {
            log.info("Sending request to: [" + watchingUrl + "]...Done. Exception [" + e.getClass().getName() + ": " + e.getMessage() + "]");
            checkResult.setResult(false);
            checkResult.setInfo("Ошибка достпности сервиса по адресу: [" + watchingUrl + "]; Exception: [" + e.getClass().getName() + ": " + e.getMessage() + "]. Будет предпринята попытка перезапуска сервиса");
        }

        return checkResult;
    }

    public void restartService() {
        log.info("Trying to run file [" + scriptFile + "]...");
        ProcessBuilder pb = new ProcessBuilder("bash", scriptFile);
        pb.inheritIO();
        try {
            Process process = pb.start();
            process.waitFor();
        } catch (Exception e) {
            log.info("Trying to run file [" + scriptFile + "]... Done. Exception [" + e.getClass().getName() + ": " + e.getMessage() + "]");
        }
        log.info("Trying to run file [" + scriptFile + "]... Done");
    }
}

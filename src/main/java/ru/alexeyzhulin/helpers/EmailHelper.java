package ru.alexeyzhulin.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;

@Component
public class EmailHelper {

    private static final Logger log = LoggerFactory.getLogger(EmailHelper.class);

    public static String FROM_ALIAS = "Электронные компоненты";

    @Value("${jdbcConnectionString:#{null}}")
    private String jdbcConnectionString;
    @Value("${databaseUserName:#{null}}")
    private String databaseUserName;
    @Value("${databaseUserPassword:#{null}}")
    private String databaseUserPassword;
    @Value("${emailRecipientsQuery:#{null}}")
    private String emailRecipientsQuery;

    @Value("${smtpServer:#{null}}")
    private String smtpServer;
    @Value("${smtpPort:#{null}}")
    private String smtpPort;
    @Value("${smtpUserName:#{null}}")
    private String smtpUserName;
    @Value("${smtpPassword:#{null}}")
    private String smtpPassword;
    @Value("${emailFrom:#{null}}")
    private String emailFrom;
    @Value("${emailField:#{null}}")
    private String emailField;

    // Send alert messages
    public void sendAlertMessage(String emailSubject, String emailMessage) throws SQLException, UnsupportedEncodingException, MessagingException {
        ArrayList<String> recipients = getRecipientList();
        for (String recipent: recipients
             ) {
            log.info("Sending message to [" + recipent + "]...");
            processMessage(emailSubject, emailMessage, recipent);
            log.info("Sending message to [" + recipent + "]... Done");
        }
    }

    // Get recipient email list
    ArrayList<String> getRecipientList() throws SQLException {
        ArrayList<String> recipients = new ArrayList<>();

        try (
                // Connect to the database. Connection is closed automatically
                Connection connection = DriverManager.getConnection(
                        jdbcConnectionString, databaseUserName, databaseUserPassword
                )
        ) {
            String queryString = emailRecipientsQuery;
            Statement statement = connection.createStatement();
            log.info("Starting query for recipient list [" + queryString + "]...");
            ResultSet resultSet = statement.executeQuery(queryString);
            log.info("Query done.");
            while (resultSet.next()) {
                recipients.add(resultSet.getString(emailField));
            }
        }
        return recipients;
    }

    // Email sending procedure
    private void processMessage(String emailSubject, String emailMessage, String emailTo) throws UnsupportedEncodingException, MessagingException {
        Session session = createMailSession();
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(emailFrom, FROM_ALIAS));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(emailTo));
        message.setSubject(emailSubject, "UTF-8");
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String messageBody = emailMessage;

        message.setText(messageBody);
        message.setContent(messageBody, "text/html; charset=utf-8");

        Transport.send(message);

    }

    private Session createMailSession() {
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.socketFactory.port", smtpPort);
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", smtpPort);

        props.setProperty("mail.smtp.ssl.enable", "true");
        props.setProperty("mail.smtp.starttls.enable", "false");
        props.setProperty("mail.smtp.ssl.trust", "*");
        props.setProperty("mail.debug", "false");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(smtpUserName, smtpPassword);
                    }
                });
        return session;
    }
}
